# `uptg`

Sends you a Telegram message as soon as your Upwork RSS gets updated!

---

Basically what the subtitle says.

Just edit your `config.toml` from the example provided and you're good to go.

## How to get your Telegram ID

Send a message to `@userinfobot` and grab your ID from there.
