package main

import (
	"flag"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/gsora/uptg/config"
	"gitlab.com/gsora/uptg/uprss"
)

var (
	cfgFilePath string
	cfg         config.Config
	err         error
	bot         *tgbotapi.BotAPI
	w           *uprss.Watcher
)

func main() {
	setupCLIParams()

	cfg, err = config.ReadConfig(cfgFilePath)
	if err != nil {
		log.Fatal(err)
	}

	w, err = uprss.NewWatcher(cfg.UpworkURL)
	if err != nil {
		log.Fatal(err)
	}

	bot, err = tgbotapi.NewBotAPI(cfg.TelegramBotAPIKey)
	if err != nil {
		log.Fatal(err)
	}

	_ = bot

	for title, _ := range w.LastJobs() {
		log.Println(title)
	}

	log.Println("uptg started!")
	for newData := range w.JobsChannel {
		if newData.UpdateError != nil {
			log.Println(newData.UpdateError)
		}

		for range newData.NewJobPosts {
			log.Printf("found %d new job(s)!", len(newData.NewJobPosts))
		}
	}
}

func setupCLIParams() {
	flag.StringVar(&cfgFilePath, "config", "./config.toml", "configuration file path")
	flag.Parse()
}
