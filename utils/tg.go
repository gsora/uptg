package utils

import (
	"fmt"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/gsora/uptg/uprss"
)

func SendTelegramMessage(j uprss.JobPost, dest int64, botAPI *tgbotapi.BotAPI) error {
	// send formatted message first...
	msg := buildMessageForJob(j, dest)
	_, originalSendErr := botAPI.Send(msg)

	var apologySendErr error
	//...if there was an error, just send the link with an apology message
	if originalSendErr != nil {
		msg := buildMessage(fmt.Sprintf(apologyLink, j.Title, j.Link), dest)
		_, apologySendErr = botAPI.Send(msg)
	}

	// again, even the apology message fails, log the error on cli and move on
	if apologySendErr != nil {
		return fmt.Errorf("failed to send message for job post %s - bubbled error: %s",
			j.Link,
			originalSendErr.Error(),
		)
	}

	return nil
}

func buildMessageForJob(j uprss.JobPost, dest int64) tgbotapi.MessageConfig {
	return buildMessage(j.HTML(), dest)
}

func buildMessage(s string, dest int64) tgbotapi.MessageConfig {
	msg := tgbotapi.NewMessage(dest, s)
	msg.ParseMode = tgbotapi.ModeHTML
	return msg
}

func SendTelegramErrorMessage(err error, dest int64, botAPI *tgbotapi.BotAPI) error {
	msg := buildMessage(
		fmt.Sprintf(
			"There was an error processing a data update: %s",
			err.Error()),
		dest,
	)

	_, sendErr := botAPI.Send(msg)

	return sendErr
}
