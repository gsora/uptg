package utils

const apologyLink string = `There was an error while processing this job posting :(

I wasn't able to successfully send you a properly-formatted Markdown message, but anyway here's the job link:

[%s](%s)`
