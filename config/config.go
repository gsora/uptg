package config

import (
	"fmt"
	"github.com/pelletier/go-toml"
	"os"
)

type Config struct {
	UpworkURL         string
	TelegramBotAPIKey string
	TelegramRecipient int64
}

func ReadConfig(path string) (c Config, err error) {
	var f *os.File
	f, err = os.Open(path)
	if err != nil {
		return
	}

	td := toml.NewDecoder(f)
	err = td.Decode(&c)
	if err != nil {
		return
	}

	if empty(c.UpworkURL) {
		return Config{}, errorMsg("missing Upwork RSS url")
	} else if empty(c.TelegramBotAPIKey) {
		return Config{}, errorMsg("missing Telegram Bot API key")
	} else if c.TelegramRecipient == 0 {
		return Config{}, errorMsg("missing Telegram recipient ID")
	}

	return
}

func errorMsg(s string) error {
	return fmt.Errorf("malformed configuration file: %s", s)
}

func empty(s string) bool {
	return s == ""
}
