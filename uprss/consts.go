package uprss

import "time"

const budgetStrRegexp string = `<b>Budget<\/b>: \$(.+(\r\n|\r|\n))<br \/>`
const categoryStrRegexp string = `<b>Category<\/b>: (.+(\r\n|\r|\n))<br \/>`
const countryStrRegexp string = `<b>Country<\/b>: (.+)<br \/>`

const intervalMultiplier time.Duration = 25
const updateInterval = intervalMultiplier * time.Second

const markdownTemplate string = `*%s*

*Description*
%s

*Budget*: $%s

*Posted on*: %s

*Country*: %s

[Link](%s)`
