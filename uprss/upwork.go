package uprss

import (
	"encoding/xml"
	"fmt"
	"html"
	"io"
	"net/http"
	"regexp"
	"strings"

	strip "github.com/grokify/html-strip-tags-go"
)

type Watcher struct {
	rssURL         string
	JobsChannel    chan UpdatePayload
	budgetRegexp   *regexp.Regexp
	categoryRegexp *regexp.Regexp
	countryRegexp  *regexp.Regexp
	lastJobs       map[string]JobPost
}

func (w Watcher) LastJobs() map[string]JobPost {
	return w.lastJobs
}

func NewWatcher(RSSUrl string) (*Watcher, error) {
	w := Watcher{
		rssURL:         RSSUrl,
		budgetRegexp:   regexp.MustCompile(budgetStrRegexp),
		categoryRegexp: regexp.MustCompile(categoryStrRegexp),
		countryRegexp:  regexp.MustCompile(countryStrRegexp),
		JobsChannel:    make(chan UpdatePayload),
	}

	// populate lastJobs
	var err error
	w.lastJobs, err = w.getPosts()
	if err != nil {
		return &Watcher{}, err
	}

	go w.grabUpdates()

	return &w, nil
}

func (w Watcher) rawJobsArrayToMap(jobs []rawJobPost) map[string]JobPost {
	jm := make(map[string]JobPost)
	for _, job := range jobs {
		jm[strings.ToUpper(job.Title)] = w.assembleJobPost(job)
	}

	return jm
}

func (w Watcher) getPosts() (map[string]JobPost, error) {
	data, err := w.fetchRSSData()
	if err != nil {
		return nil, err
	}

	var jpc jobPostContainer
	dec := xml.NewDecoder(data)
	err = dec.Decode(&jpc)

	jobs := w.rawJobsArrayToMap(jpc.JobPosts)

	return jobs, nil
}

func (w Watcher) fetchRSSData() (io.ReadCloser, error) {
	resp, err := http.Get(w.rssURL)
	if err != nil {
		return nil, err
	}

	return resp.Body, nil
}

func (w Watcher) assembleJobPost(post rawJobPost) (j JobPost) {
	j = w.parseRawCDATA(post.Description)

	j.Title = strings.Split(post.Title, " - Upwork")[0]

	j.HTMLDescription = fmt.Sprintf("<b>%s</b>\n\n%s",
		j.Title,
		strings.Replace(post.Description, "<br />", "\n", -1),
	)

	rawCleanedDescr := strip.StripTags(post.Description)

	// real description body always ends with "Budget" - split by it and take the first part!
	j.Description = strings.Split(rawCleanedDescr, "Budget")[0]

	// unescape HTML
	j.Description = html.UnescapeString(j.Description)

	// replace any * with -
	j.Description = strings.Replace(j.Description, "*", "-", -1)

	j.Link = post.Link
	j.PostedOn = post.PostedOn
	return
}

func (w *Watcher) parseRawCDATA(cdata string) (j JobPost) {
	f := func(s []string) string {
		if len(s) >= 2 {
			return s[1]
		}

		return ""
	}

	j.Budget = f(w.budgetRegexp.FindStringSubmatch(cdata))
	j.Category = f(w.categoryRegexp.FindStringSubmatch(cdata))
	j.Country = f(w.countryRegexp.FindStringSubmatch(cdata))

	return
}
