package uprss

import (
	"encoding/xml"
	"fmt"
)

type rawJobPost struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	PostedOn    string `xml:"pubDate"`
}

type jobPostContainer struct {
	XMLName  xml.Name     `xml:"rss"`
	JobPosts []rawJobPost `xml:"channel>item"`
}

type JobPost struct {
	Title           string
	Link            string
	Description     string
	HTMLDescription string
	Budget          string
	PostedOn        string
	Category        string
	Country         string
}

func (j JobPost) Equals(nj JobPost) bool {
	return j.Title == nj.Title
}

func (j JobPost) HTML() string {
	return j.HTMLDescription
}

func (j JobPost) Markdown() string {
	return fmt.Sprintf(
		markdownTemplate,
		j.Title,
		j.Description,
		j.Budget,
		j.PostedOn,
		j.Country,
		j.Link,
	)
}
