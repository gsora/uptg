package uprss

import (
	"strings"
	"time"
)

type UpdatePayload struct {
	UpdateError error
	NewJobPosts []JobPost
}

func (w *Watcher) grabUpdates() {
	for {
		time.Sleep(updateInterval)

		jobs, err := w.getPosts()
		if err != nil {
			w.JobsChannel <- UpdatePayload{
				err,
				[]JobPost{},
			}
			continue
		}

		nj := w.buildNewPostArray(jobs)

		// if there are new jobs, save `jobs` as the new `lastJobs`
		if len(nj) > 0 {
			w.JobsChannel <- UpdatePayload{
				nil,
				nj,
			}
			w.lastJobs = jobs
		}
	}
}

func (w *Watcher) buildNewPostArray(newPosts map[string]JobPost) (nj []JobPost) {
	// a job is considered new when the map lookup for its title is
	// empty.
	// then, add it to nj.
	for title, newJob := range newPosts {
		if _, found := w.lastJobs[strings.ToUpper(title)]; !found {
			nj = append(nj, newJob)
		}
	}

	return
}
